(function($) {
  // $('.accordion > li:eq(0) a').addClass('active').next().slideDown();

  if ($('#accordion_')) {

    $('ul#accordion_').each(function( index ) {
      $(this).attr('id', ("accordion_" + index));

    });
  }

  $('.accordion.multiple-expands a').click(function(j) {
    var dropDown = $(this).closest('li').find('p'),
        slideTime = 0.3;

    if ($(this).hasClass('active'))  {
      $(this).removeClass('active');
    } else {
      $(this).addClass('active');
    }

    dropDown.stop(false, true).slideToggle(slideTime);

    j.preventDefault();
  });

  $('.accordion.single-expand a.project').click(function(j) {

    var dropDown = $(this).closest('li').find('p'),
        slideTime = 0.3;

    if ($(this).hasClass('project'))  {
      $(this).toggleClass('changed');
      $(this).removeClass('project');
    } else {
      $(this).toggleClass('changed');
      $(this).addClass('project');
    }

    if ($(this).hasClass('active'))  {
      $(this).removeClass('active');
      $(this).closest('li').removeAttr('id');
      $(this).addClass('project');
    } else {
      $(this).closest('.accordion').find('a.active').removeClass('active').addClass('project').toggleClass('changed');
      $(this).closest('.accordion').find('li#scrolling').removeAttr('id');
      $(this).closest('.accordion').find('p').not(dropDown).slideUp(slideTime);
      $(this).addClass('active');
      $(this).closest('li').attr('id', 'scrolling');
      setTimeout(function() {
        $('#scrolling').ScrollTo();;
      }, 50);
    }

    dropDown.stop(false, true).slideToggle(slideTime);

    j.preventDefault();
  });

  $('.accordion.single-expand a.active').click(function(j) {

    var dropDown = $(this).closest('li').find('p'),
        slideTime = 0.3;

    if ($(this).hasClass('project'))  {
      $(this).toggleClass('changed');
      $(this).removeClass('project');
    } else {
      $(this).toggleClass('changed');
      $(this).addClass('project');
    }

    if ($(this).hasClass('active'))  {
      $(this).removeClass('active');
      $(this).closest('li').removeAttr('id');
      $(this).addClass('project');
    } else {
      $(this).closest('.accordion').find('a.active').removeClass('active').addClass('project').toggleClass('changed');
      $(this).closest('.accordion').find('li#scrolling').removeAttr('id');
      $(this).closest('.accordion').find('p').not(dropDown).slideUp(slideTime);
      $(this).addClass('active');
      $(this).closest('li').attr('id', 'scrolling');
      setTimeout(function() {
        $('#scrolling').ScrollTo();;
      }, 50);
    }

    dropDown.stop(false, true).slideToggle(slideTime);

    j.preventDefault();
  });

})(jQuery);
