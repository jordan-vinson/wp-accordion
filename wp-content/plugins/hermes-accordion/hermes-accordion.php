<?php
/*
Plugin Name: Accordion
Plugin URI: http://hermesdevelopment.com/
Description: Declares a plugin that will create a custom post type displaying an accordion.
Version: 1.0
Author: Hermes Development
Author URI: http://hermesdevelopment.com
License: GPLv2
*/

function hermes_enqueue() {
    wp_register_style('hermes-accordion-style', plugins_url('hermes-accordion/css/style.css'));
    wp_enqueue_style('hermes-accordion-style');


    wp_register_script('hermes-custom-js', plugins_url('hermes-accordion/js/board.js'), array('jquery'), '2.1', true);
    wp_enqueue_script('hermes-custom-js');

    wp_register_script('hermes-custom-js2', plugins_url('hermes-accordion/js/jquery-scrollto.js'), array('jquery'), '2.1', true);
    wp_enqueue_script('hermes-custom-js2');


}
add_action( 'wp_enqueue_scripts', 'hermes_enqueue' );

add_action( 'init', 'create_character' );




function create_character() {
  register_post_type( 'character',
      array(
          'labels' => array(
              'name' => 'Characters',
              'singular_name' => 'Character',
              'add_new' => 'Add New',
              'add_new_item' => 'Add New Character',
              'edit' => 'Edit',
              'edit_item' => 'Edit Character',
              'new_item' => 'New Character',
              'view' => 'View',
              'view_item' => 'View Character',
              'search_items' => 'Search Character',
              'not_found' => 'No Character found',
              'not_found_in_trash' => 'No Character found in Trash',
              'parent' => 'Parent Character'
          ),

          'public' => true,
          'menu_position' => 15,
          'supports' => array( 'title', 'editor', 'thumbnail', 'custom-fields' ),
          'taxonomies' => array( '' ),
          'menu_icon' => plugins_url( 'images/boardmember.png', __FILE__ ),
          'has_archive' => true
      )
  );
}

add_shortcode('accordion', function($args) {
    // arguments
    $post_type = $args['type'];
    $post_type = $post_type == 'entry' ? 'entry' : $post_type;
    $post_type = $post_type == 'character' ? 'character' : $post_type;
    $post_type = $post_type == '' ? 'character' : $post_type; // default value


    $has_anchor = $args['anchor'];
    $has_anchor = $has_anchor == 'yes' || $has_anchor == 'y' ? 'yes' : $has_anchor;
    $has_anchor = $has_anchor == 'no' || $has_anchor == 'n' ? 'no' : $has_anchor;
    $has_anchor = $has_anchor == '' ? 'no' : $has_anchor;


    $posts = get_posts( array(
        'numberposts' => 10,
        'orderby' => 'menu_order',
        'order' => 'ASC',
        'post_type' => $post_type
    )); //array of objects returned

    if ($has_anchor == 'yes') {
        $parent_el  = $post_type == 'featured_projects' ? '<ul class="accordion single-expand featured-image" id="accordion_" >' : '<ul class="accordion multiple-expands" id="accordion_">'; //Open the ul
      } else {
        $parent_el  = $post_type == 'entry' ? '<ul class="accordion multiple-expands data-no-instant">' : '<ul class="accordion multiple-expands data-no-instant character-accordion">'; //Open the ul
      }

    if ($post_type == 'character') {

      foreach ( $posts as $post ) { // Generate the markup for each parent_el member
        $parent_el .= sprintf(('	<li><a class="no-image" data-no-instant>%1$s</a>%2$s</li>'),
          $post->post_title,
          wpautop($post->post_content)
        );
      }
    }



   $parent_el .= '</ul>'; //Close the container

   return $parent_el; //Return the HTML.
});
